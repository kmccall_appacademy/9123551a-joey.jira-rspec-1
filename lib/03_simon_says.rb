def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, number = 2)
  result = [word] * number
  result.join(' ')
end

def start_of_word(word, number = 1)
  word.chars.take(number).join
end

def first_word(word)
  word.split[0]
end

def titleize(word)
  result = []
  small_words = %w(a an the and but or for of over)
  string_arr = word.split
  string_arr.each_with_index do |el, idx|
    if small_words.include?(el) && idx != 0
      result << el.downcase
    else
      result << el.capitalize
    end
  end

  result.join(' ')
end
