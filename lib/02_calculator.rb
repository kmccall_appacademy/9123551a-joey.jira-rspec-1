def add(int1, int2)
  int1 + int2
end

def subtract(int1, int2)
  int1 - int2
end

def sum(array)
  array.reduce(:+) || 0
end

def multiply(*int)
  int.reduce(:*)
end

def power(int1, int2)
  int1**int2
end

def factorial(int)
  int.downto(1).reduce(:*) || 0
end
