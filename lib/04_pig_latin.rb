def translate(word)
  result = []

  word.scan(/\w+/) do |el|
    if el.match(/\Aqu/)
      result << el[2..-1] + "quay"
    elsif el.match(/\A[^aeiou]qu/)
      result << el[3..-1] + el[0..2] + "ay"
    elsif el.match(/\A[aeiou]/)
      result << el + "ay"
    else
      result << el[/[aeiou].*/] + el[/\A[^aeiou]*/] + "ay"
    end
  end

  result.join(" ")
end
